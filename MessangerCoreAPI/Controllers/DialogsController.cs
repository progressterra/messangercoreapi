﻿using System;
using System.Collections.Generic;
using MessangerCoreAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MessangerCoreAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DialogsController : ControllerBase
    {
        private readonly IDialogsService _dialogsService;
        private readonly ILogger<DialogsController> _logger;

        public DialogsController(IDialogsService dialogsService, ILogger<DialogsController> logger)
        {
            _dialogsService = dialogsService;
            _logger = logger;
        }

        /// <summary>
        /// Получает идентификатор диалога, в котором есть все переданные клиенты.
        /// </summary>
        /// <param name="guidClients">Список идентификаторов клиентов</param>
        /// <returns>Если диалог найден возвращается идентификатор диалога, иначе возвращается пустой GUID</returns>
        [HttpGet]
        public Guid Get([FromQuery] List<Guid> guidClients)
        {
            return _dialogsService.GetByGuidClients(guidClients);
        }
    }
}
