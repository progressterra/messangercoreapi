﻿using System;
using System.Collections.Generic;
using System.Linq;
using MessangerCoreAPI.Models;

namespace MessangerCoreAPI.Repositories
{
    public class DialogsRepositoriy : IDialogsRepositoriy
    {
        public Guid GetByGuidClients(List<Guid> guidClients)
        {
            return RGDialogsClients.Init()
                .Where(_ => guidClients.Contains(_.IDClient))
                .GroupBy(_ => _.IDRGDialog, _ => _.IDClient)
                .Select(_ => new { _.Key, Count = _.Count() })
                .Where(_ => _.Count == guidClients.Count)
                .Select(_ => _.Key)
                .FirstOrDefault();
        }
    }
}
