﻿using System;
using System.Collections.Generic;

namespace MessangerCoreAPI.Repositories
{
    public interface IDialogsRepositoriy
    {
        Guid GetByGuidClients(List<Guid> guidClients);
    }
}
