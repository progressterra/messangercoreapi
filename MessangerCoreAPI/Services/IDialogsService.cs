﻿using System;
using System.Collections.Generic;

namespace MessangerCoreAPI.Services
{
    public interface IDialogsService
    {
        Guid GetByGuidClients(List<Guid> guidClients);
    }
}
