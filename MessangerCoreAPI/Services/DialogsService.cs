﻿using System;
using System.Collections.Generic;
using MessangerCoreAPI.Repositories;

namespace MessangerCoreAPI.Services
{
    public class DialogsService : IDialogsService
    {
        private readonly IDialogsRepositoriy _repositoriy;

        public DialogsService(IDialogsRepositoriy repositoriy)
        {
            _repositoriy = repositoriy;
        }

        public Guid GetByGuidClients(List<Guid> guidClients) =>  _repositoriy.GetByGuidClients(guidClients);
    }
}
